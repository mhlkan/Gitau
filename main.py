from flask import Flask, render_template, request
from search import GSearch
app = Flask(__name__)
@app.route('/')
def index():
    return render_template('search.html')
@app.route('/search', methods=["GET", "POST"])
def search():
    if request.method == "POST":
        kw = request.form.get('kw')
        return render_template(
            'search.html',
            results = GSearch(kw).mix
        )
    else:
        return index()
if __name__ == '__main__':
    app.run(debug=True)