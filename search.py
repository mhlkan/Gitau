from bs4 import BeautifulSoup as Soup
import itertools as it
import requests as rq
class GSearch:
    def __init__(
        self,
        kw: str
    ):
        self.keyword = kw
        self.searchl = {
            'github': "https://github.com/search?utf8=%E2%9C%93&q={}&type=",
            'gitlab': "https://gitlab.com/explore/projects?name={}",
            'bitbucket': "https://bitbucket.org/repo/all?name={}"
        }
    @property
    def github(self) -> list:
        request = rq.get(self.searchl['github'].format(self.keyword))
        html = request.content
        soup = Soup(html, 'html.parser')
        classn = 'repo-list-item d-flex flex-justify-start py-4 public source'
        rlist = []
        for div in soup.find_all('div', {'class': classn}):
            rd = {
                'name': div.find('a')['href'][1:],
                'link': 'https://github.com' + div.find('a')['href'],
                'description': div.find('p').text
            }
            rlist.append(rd)
        return rlist
    @property
    def gitlab(self) -> list:
        request = rq.get(self.searchl['gitlab'].format(self.keyword))
        html = request.content
        soup = Soup(html, 'html.parser')
        classn = 'project-details'
        rlist = []
        for div in soup.find_all('div', {'class': classn}):
            try:
                rd = {
                    'name': div.find('a')['href'][1:],
                    'link': 'https://gitlab.com' + div.find('a')['href'],
                    'description': div.find('p').text
                }
            except:
                rd = {
                    'name': div.find('a')['href'][1:],
                    'link': 'https://gitlab.com' + div.find('a')['href'],
                    'description': ''
                }
            rlist.append(rd)
        return rlist
    @property
    def bitbucket(self) -> list:
        request = rq.get(self.searchl['bitbucket'].format(self.keyword))
        html = request.content
        soup = Soup(html, 'html.parser')
        classn = 'repo-summary'
        rlist = []
        for a in soup.find_all('article', {'class': classn}):
            try:
                rd = {
                    'name': a.find('a', {'class': 'repo-link'})['href'][1:],
                    'link': 'https://bitbucket.org' + a.find('a', {'class': 'repo-link'})['href'],
                    'description': a.find('p').text
                }
            except:
                rd = {
                    'name': a.find('a', {'class': 'repo-link'})['href'][1:],
                    'link': 'https://bitbucket.org' + a.find('a', {'class': 'repo-link'})['href'],
                    'description': ''
                }
            rlist.append(rd)
        return rlist
    @property
    def mix(self) -> list:
        return list(it.chain.from_iterable(zip(list(it.chain.from_iterable(zip(self.github, self.gitlab))))))
